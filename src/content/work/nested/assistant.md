---
title: Vendeur virtuel
publishDate: 2019-10-02 00:00:00
img: /assets/chatbot.jpg
img_alt: Soft pink and baby blue water ripples together in a subtle texture.
description: |
  Lors d'un stage chez Mad Impact, j'ai collaborer à la création d'un assistant de vente virtuel propulsé par intelligence artificiel.
tags:
  - Machine Learning
  - Python
  - SpaCy
---


## Assistant de vente

En tant que stagiaire chez Mad Impact, j’ai eu l’opportunité de contribuer au développement d’un Assistant de Vente Virtuel, une extension destinée à améliorer l’expérience d’achat en ligne pour les boutiques. Mon rôle a consisté à concevoir et à développer le backend de l’assistant, en utilisant des technologies telles que Flask pour l’API RESTful, Elasticsearch pour la recherche de données, et SpaCy pour le traitement avancé des requêtes clients. J’ai également intégré l’API OpenAI pour fournir des réponses personnalisées et pertinentes aux utilisateurs, en mettant l’accent sur la réduction des coûts et l’optimisation des performances. Ce projet m’a permis de renforcer mes compétences en développement Python et en intégration de services IA, tout en contribuant à un produit innovant au sein d’une entreprise en pleine croissance technologique.

### Compétences mises en oeuvre

- Réaliser un développement d’application (UE1)

- Optimiser des applications informatiques (UE2)

- Gérer des données de l’information (UE4)

- Conduire un projet (UE5)

- Travailler dans une équipe informatique (UE6)